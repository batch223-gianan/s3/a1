<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s3 a1</title>
</head>
<body>
    <h1>Person</h1>
   <?php $person = new Person("Senku"," ", "Ishigami") ?>
   <p><?= $person->printName() ?></p>

   <h1>Developer</h1>
   <?php $developer = new Developer("John", "Finch", "Smith") ?>
   <p><?= $developer->printName() ?></p>

   <h1>Engineer</h1>
   <?php $engineer = new Engineer("Harold", "Myers", "Reese") ?>
   <p><?= $engineer->printName() ?></p>

</body>
</html>